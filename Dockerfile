FROM debian:10 as build
RUN apt-get update && apt install -y wget gcc make
RUN apt-get install -y libpcre3-dev libpcre3 zlib1g-dev
RUN wget https://nginx.org/download/nginx-1.22.1.tar.gz && tar xvfz nginx-1.22.1.tar.gz && cd nginx-1.22.1  && ./configure && make && make install

FROM debian:10
RUN apt-get update -y
RUN apt install libpcre3 -y
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx /usr/local/nginx/sbin/nginx
COPY --from=build /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf
COPY --from=build /usr/local/nginx/conf/mime.types /usr/local/nginx/conf/mime.types
RUN mkdir -p /usr/local/nginx/logs
RUN useradd -r nginx
RUN chown -R nginx:nginx /usr/local/nginx/logs
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]